let interval

const clock = () => {
    const setDeg = (secDeg, minDeg, hoursDeg) => {
        document.getElementsByClassName('second-arrow')[0].style.transform = `rotate(${secDeg}deg)`
        document.getElementsByClassName('minute-arrow')[0].style.transform = `rotate(${minDeg}deg)`
        document.getElementsByClassName('hour-arrow')[0].style.transform = `rotate(${hoursDeg}deg)`
    }

    const now = new Date()
    const then = new Date(
        now.getFullYear(),
        now.getMonth(),
        now.getDate(),
        0,0,0)

    const seconds = (now.getTime() - then.getTime()) / 1000

    let secondsDeg = seconds * 6
    let minutesDeg = secondsDeg / 60
    let hoursDeg = seconds * 0.0083

    setDeg(secondsDeg, minutesDeg, hoursDeg)

    interval = setInterval(() => {
        secondsDeg+=(6/100)
        minutesDeg+=(0.1/100)
        hoursDeg+=(0.0083/100)
        setDeg(secondsDeg, minutesDeg, hoursDeg)
    }, 10)
}

clock()

window.onfocus = () => {
    clock()
}

window.onblur = () => {
    clearInterval(interval)
}



